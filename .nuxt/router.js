import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _92d711d0 = () => interopDefault(import('..\\pages\\Dashboard.vue' /* webpackChunkName: "pages_Dashboard" */))
const _44b71bac = () => interopDefault(import('..\\pages\\index2.vue' /* webpackChunkName: "pages_index2" */))
const _9037c714 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/Dashboard",
    component: _92d711d0,
    name: "Dashboard"
  }, {
    path: "/index2",
    component: _44b71bac,
    name: "index2"
  }, {
    path: "/",
    component: _9037c714,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
